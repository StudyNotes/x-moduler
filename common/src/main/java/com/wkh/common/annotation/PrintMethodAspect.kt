package com.wkh.common.annotation

import android.os.SystemClock
import android.util.Log
import org.aspectj.lang.ProceedingJoinPoint
import org.aspectj.lang.annotation.Around
import org.aspectj.lang.annotation.Aspect
import org.aspectj.lang.annotation.Pointcut
import org.aspectj.lang.reflect.MethodSignature

@Aspect
class PrintMethodAspect {

    private val tag = "PrintMethodAspect"

    @Pointcut("execution(@com.wkh.common.annotation.PrintMethod * *(..))")
    fun methodAnnotated() {

    }

    @Around("methodAnnotated()")
    fun aroundJoinPoint(point: ProceedingJoinPoint) {
        var startTime = SystemClock.currentThreadTimeMillis()
        val methodSignature = point.signature as MethodSignature
        val method = methodSignature.method

        val declaringClass = method.declaringClass

        if (method.isAnnotationPresent(PrintMethod::class.java)) {
            point.proceed()
            var execTime = SystemClock.currentThreadTimeMillis() - startTime
            Log.i(tag, "${declaringClass.simpleName} ${method.name} cost $execTime ms")
        }
    }

}
package com.wkh.common.http

import retrofit2.Retrofit

abstract class CustomHttpInit {
    abstract fun initRetrofit(): Retrofit
}
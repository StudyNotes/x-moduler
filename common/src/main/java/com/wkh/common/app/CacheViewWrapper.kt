package com.wkh.common.app

import android.view.View

/**
 * @des: view的缓存封装类
 * @author: wkh
 * @version: 1.0.0
 * @date: 3/19/21 13:46
 * @see {@link }
 */
class CacheViewWrapper(view: View) {

    var cachedView = view
    var createTime = System.currentTimeMillis()

    var next: CacheViewWrapper? = null

}
package com.wang.lib.b

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.wang.lib.a.api.A3Interface
import com.wang.lib.a.api.A4Interface
import com.wang.lib.b.model.Person
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.activity_b.*
import javax.inject.Inject

@AndroidEntryPoint
class BActivity : AppCompatActivity() {

    @Inject
    lateinit var person: Person

    @Inject
    lateinit var inter: A3Interface

    @Inject
    lateinit var inter4: A4Interface

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_b)

        testButton.setOnClickListener {
            inter.test()
            inter4.test()
        }

    }

}
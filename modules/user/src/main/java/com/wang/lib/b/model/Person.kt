package com.wang.lib.b.model

import android.util.Log
import javax.inject.Inject

class Person @Inject constructor() {

    fun sayHello() {
        Log.i("Hilt", "Person sayHello")
    }

}
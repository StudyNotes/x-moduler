package com.wang.lib.a.impl

import com.wang.lib.a.api.A3Interface
import com.wang.lib.a.api.A4Interface
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ActivityComponent

@Module
@InstallIn(ActivityComponent::class)
abstract class AnalyticsModule {

    @Binds
    abstract fun bindA3(a3Impl: A3InterfaceImple_2): A3Interface

    @Binds
    abstract fun bindA4(a3Impl: A4InterfaceImpl): A4Interface

}
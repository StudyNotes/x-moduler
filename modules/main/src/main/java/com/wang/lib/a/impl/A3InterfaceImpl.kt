package com.wang.lib.a.impl

import android.util.Log
import com.wang.lib.a.api.A3Interface
import javax.inject.Inject

class A3InterfaceImpl @Inject constructor() : A3Interface {

    override fun test() {
        Log.i("Hilt", "A3InterfaceImpl -> test")
    }
}
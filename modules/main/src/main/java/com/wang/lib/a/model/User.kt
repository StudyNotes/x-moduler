package com.wang.lib.a.model

import android.util.Log
import javax.inject.Inject

class User @Inject constructor() {

    fun sayHello() {
        Log.i("User", "Hello")
    }

}
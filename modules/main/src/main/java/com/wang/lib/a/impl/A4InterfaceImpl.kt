package com.wang.lib.a.impl

import android.util.Log
import com.wang.lib.a.api.A4Interface
import javax.inject.Inject

class A4InterfaceImpl @Inject constructor() : A4Interface {
    override fun test() {
        Log.i("Hilt", "A4InterfaceImpl -> test")
    }
}
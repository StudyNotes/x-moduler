package com.wang.lib.a.impl

import android.util.Log
import com.wang.lib.a.api.A3Interface
import javax.inject.Inject

class A3InterfaceImple_2 @Inject constructor() : A3Interface {
    override fun test() {
        Log.i("Hilt", "A3InterfaceImple_2 -> test")
    }
}
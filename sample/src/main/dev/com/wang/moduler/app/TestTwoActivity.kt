package com.wang.moduler.app

import android.os.Bundle
import android.os.SystemClock
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import androidx.asynclayoutinflater.view.AsyncLayoutInflater
import com.wang.moduler.R
import com.wkh.common.annotation.PrintMethod

/**
 * @des:
 * @author: wkh
 * @version: 1.0.0
 * @date: 3/18/21 19:27
 * @see {@link }
 */
class TestTwoActivity : AppCompatActivity() {

    @PrintMethod
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val startTime = SystemClock.currentThreadTimeMillis()
        AsyncLayoutInflater(this).inflate(R.layout.activity_layout_test, null) { view, _, _ ->
            val endTime = SystemClock.currentThreadTimeMillis()
            Log.i("PrintMethodAspect", "线程加载耗时 = ${endTime - startTime}")
            setContentView(view)
        }
    }

}
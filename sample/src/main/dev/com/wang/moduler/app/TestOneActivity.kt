package com.wang.moduler.app

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.wang.moduler.R
import com.wkh.common.annotation.PrintMethod

/**
 * @des:
 * @author: wkh
 * @version: 1.0.0
 * @date: 3/18/21 19:27
 * @see {@link }
 */
class TestOneActivity : AppCompatActivity() {

    @PrintMethod
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_layout_test)
    }

}
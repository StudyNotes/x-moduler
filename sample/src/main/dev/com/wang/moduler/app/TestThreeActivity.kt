package com.wang.moduler.app

import android.os.Bundle
import android.os.SystemClock
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import com.wang.moduler.R
import com.wkh.common.annotation.PrintMethod
import kotlinx.coroutines.*

/**
 * @des:
 * @author: wkh
 * @version: 1.0.0
 * @date: 3/18/21 19:27
 * @see {@link }
 */
class TestThreeActivity : AppCompatActivity(), CoroutineScope by MainScope() {

    @PrintMethod
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        launch {
            val startTime = SystemClock.currentThreadTimeMillis()
            println("Default               : I'm working in thread ${Thread.currentThread().name}")

            Log.i("PrintMethodAspect", "I'm working in thread ${Thread.currentThread().name}")
            val endTime = SystemClock.currentThreadTimeMillis()
            Log.i("PrintMethodAspect", "协程加载耗时 = ${endTime - startTime}")
            setContentView(getView())
        }
    }

    private suspend fun getView(): View {
        delay(2000)
        println("getView               : I'm working in thread ${Thread.currentThread().name}")
        return LayoutInflater.from(this).inflate(R.layout.activity_layout_test, null)
    }

}
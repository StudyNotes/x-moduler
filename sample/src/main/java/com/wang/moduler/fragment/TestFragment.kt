package com.wang.moduler.fragment

import com.wang.moduler.R
import com.wkh.common.app.BaseFragment

/**
 * @des:
 * @author: wkh
 * @version: 1.0.0
 * @date: 3/18/21 22:43
 * @see {@link }
 */
class TestFragment : BaseFragment(R.layout.activity_layout_test, true) {

    override fun initView() {
    }

}
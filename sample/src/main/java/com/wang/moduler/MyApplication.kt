package com.wang.moduler

import android.app.Application
import android.view.LayoutInflater
import com.wkh.common.app.LayoutHelper
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class MyApplication : Application() {

    override fun onCreate() {
        super.onCreate()

        LayoutHelper.get().init(this)

    }

}
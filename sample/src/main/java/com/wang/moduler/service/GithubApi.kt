package com.wang.moduler.service

import com.wang.moduler.model.Repos
import retrofit2.http.GET

interface GithubApi {

    @GET("/repos/octocat/Hello-World")
    suspend fun getReposWithCoroutine(): Repos

}
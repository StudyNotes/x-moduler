package com.wang.moduler.model

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class ReposViewModel : ViewModel() {

    var reposModel: MutableLiveData<Repos> = MutableLiveData()

    fun updateData(repos: Repos) {
        reposModel.value = repos
    }

}
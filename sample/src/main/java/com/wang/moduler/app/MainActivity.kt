package com.wang.moduler.app

import android.content.Intent
import android.util.Log
import android.view.View
import com.wang.lib.b.BActivity
import com.wang.moduler.R
import com.wkh.common.annotation.PrintMethod
import com.wkh.common.app.BaseActivity
import kotlinx.android.synthetic.main.activity_main.view.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import java.lang.Exception
import java.lang.NullPointerException

class MainActivity : BaseActivity(R.layout.activity_main, true) {

    override fun initView() {
        rootView.skipButton.setOnClickListener {
//            startB()
            test2()
        }

        rootView.secondButton.setOnClickListener {
            var intent = Intent(this, BActivity::class.java)
            startActivity(intent)
        }

        rootView.parseLayout.setOnClickListener {
            var intent = Intent(this, CoroutineTestActivity::class.java)
            startActivity(intent)
        }

        rootView.startFragment.setOnClickListener {
            var intent = Intent(this, FragmentContainerActivity::class.java)
            startActivity(intent)
        }
    }

    private fun startB() {
        var intent = Intent(this, TestActivity::class.java)
        startActivity(intent)
    }

    private fun test2(){
        GlobalScope.launch {
            println("父协程开启")

            GlobalScope.launch {
                println("子协程2开始执行")
                delay(500)
                println("子协程2执行完毕")
            }

            GlobalScope.launch {
                println("子协程1开始执行")
                try {
                    throw NullPointerException()
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        }
    }

    private fun test() {
        launch {
            println("父协程开启")

            launch {
                println("子协程2开始执行")
                delay(500)
                println("子协程2执行完毕")
            }

            launch(Dispatchers.IO) {
                println("子协程1开始执行")
                try {
                    throw NullPointerException()
                } catch (e: Exception) {
                    e.printStackTrace()
                }

            }

            println("父协程执行完毕")
        }
    }

}

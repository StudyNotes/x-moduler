package com.wang.moduler.app

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import androidx.appcompat.app.AppCompatActivity
import com.wang.moduler.R
import com.wang.moduler.fragment.TestFragment
import com.wkh.common.annotation.PrintMethod
import kotlinx.android.synthetic.main.activity_choice_type.*
import kotlinx.coroutines.*

/**
 * @des: 使用协程异步解析布局文件
 * @author: wkh
 * @version: 1.0.0
 * @date: 3/18/21 19:03
 * @see {@link }
 */
class CoroutineTestActivity : AppCompatActivity(), CoroutineScope by MainScope() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_choice_type)

        button.setOnClickListener {
//            var intent = Intent(this, TestOneActivity::class.java)
//            startActivity(intent)
        }
        button2.setOnClickListener {
//            var intent = Intent(this, TestTwoActivity::class.java)
//            startActivity(intent)
        }
        button3.setOnClickListener {
            inflaterView()
        }

    }


    @PrintMethod
    private fun inflaterView() {
        var view = LayoutInflater.from(this).inflate(R.layout.fragment_base, null)
    }

}
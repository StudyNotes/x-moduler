package com.wang.moduler.app

import com.wang.moduler.R
import com.wang.moduler.fragment.TestFragment
import com.wkh.common.annotation.PrintMethod
import com.wkh.common.app.BaseActivity
import kotlinx.coroutines.launch

/**
 * @des:
 * @author: wkh
 * @version: 1.0.0
 * @date: 3/19/21 11:00
 * @see {@link }
 */
class FragmentContainerActivity : BaseActivity(R.layout.actiivty_fragment_container) {

    @PrintMethod
    override fun initView() {
        launch {
            val beginTransaction = supportFragmentManager.beginTransaction()
            beginTransaction.replace(R.id.rootFrameLayout, TestFragment()).commit()
        }
    }
}

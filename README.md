# xModuler

#### 介绍
基于kotlin+androidx+jetpack打造的更佳灵活且敏捷的模块化开发方案

#### 软件架构
MVVM+MVP混合方案


#### 技术栈

1.  androidx
2.  jetpack
3.  kotlin协程



#### 其他

1.  博客地址：https://www.jianshu.com/u/58b9f1425318
